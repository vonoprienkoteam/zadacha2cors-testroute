var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  var a = parseInt(req.query.a) || 0,
      b = parseInt(req.query.b) || 0;
  res.send((a + b).toString());
});

module.exports = router;
